import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';




@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

 
  constructor
  (
   public router : Router
  ) 
  { }

  ngOnInit() {
    // $(window).load(function() {
    //   $("html, body").animate({ scrollTop:2000 }, 4000);
    // });
  }

  scrollTotop(){
    // var elmnt = document.getElementById("mycontent");
    // elmnt.scrollLeft = 50;
    // elmnt.scrollTop = 10;
  //   $("#button").click(function() {
  //     $('html').animate({
  //         scrollTop: $(".container").offset().top
  //     }, 2000);
  // });
  }

  openLink(name){
    if(name == 'fb'){
      window.open('https://www.facebook.com/FanCardApp/','_blank')
    }
    else if(name == 'tw'){
      window.open('https://twitter.com/FanCardApp','_blank')
    }else {
      window.open('https://www.youtube.com/watch?v=_TdsR5WeEEY&feature=youtu.be','_blank')
    }
  }


}
