import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
declare var $: any;
declare var jquery: any;
@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

	constructor
		(
			public router: Router
		) { }

	ngOnInit() {
		// When the user scrolls the page, execute myFunction 
		window.onscroll = function () { myFunction() };

		// Get the header
		var header = document.getElementById("mynavbar");

		// Get the offset position of the navbar
		var sticky = header.offsetTop;

		// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
		function myFunction() {
			if (window.pageYOffset > sticky) {
				header.classList.add("sticky");
			} else {
				header.classList.remove("sticky");
			}
		}
	}

	showhide() {
		$('.navbar-nav>li>a').on('click', function () {
			$('.collapse').collapse();
		});
	}

	// schoolList() {
	// 	// // Scroll certain amounts from current position 
	// 	if (this.router.url === '/home') {
	// 		window.scrollBy({
	// 			top: 600, // could be negative value
	// 			left: 0,
	// 			behavior: 'smooth'
	// 		});
	// 		document.querySelector('.container').scrollIntoView({
	// 			behavior: 'smooth'
	// 		});
	// 	} else {
	// 		this.router.navigateByUrl('/home');
	// 		window.scrollBy({
	// 			top: 600, // could be negative value
	// 			left: 0,
	// 			behavior: 'smooth'
	// 		});
	// 		document.querySelector('.container').scrollIntoView({
	// 			behavior: 'smooth'
	// 		});
	// 	}
	// }

	// dealsList() {
	// 	if (this.router.url === '/home') {
	// 		window.scrollBy({
	// 			top: 1500, // could be negative value
	// 			left: 0,
	// 			behavior: 'smooth'
	// 		});
	// 		document.querySelector('.container').scrollIntoView({
	// 			behavior: 'smooth'
	// 		});
	// 	} else {
	// 		this.router.navigateByUrl('/home');
	// 		window.scrollBy({
	// 			top: 1500, // could be negative value
	// 			left: 0,
	// 			behavior: 'smooth'
	// 		});
	// 		document.querySelector('.container').scrollIntoView({
	// 			behavior: 'smooth'
	// 		});
	// 	}
	// }

}
