export class constants {
  // http://18.217.214.199/admin-panel/Webservice/Category
  // public static BASE_URL = "https://www.fancard.app/admin-panel/";

  public static BASE_URL = "http://fancard.excellentwebworld.in/admin-panel/";
  public static BASE_URL_TYPE = "Webservice/";
  public static LOGIN = constants.BASE_URL_TYPE + "Login";
  public static OTPFORREGISTERATION = constants.BASE_URL_TYPE + "OtpForRegister";

  public static REGISTERATION = constants.BASE_URL_TYPE + "Register";

  public static CATEGORIES = constants.BASE_URL_TYPE + "Category";
  public static SCHOOLLISTFROMLOCATION = constants.BASE_URL_TYPE + "schoollistForWebsite";

  public static SPONSORED_BUSINESS = constants.BASE_URL_TYPE + "SponsoredBusiness";

  public static SCHOOLDETAIL = constants.BASE_URL_TYPE + "getSchoolDetails/";
  public static BUSINESSLIST = constants.BASE_URL_TYPE + "BusinessData";

  public static BUSINESS_DETAIL = constants.BASE_URL_TYPE + "BusinessDetails/";
  public static CONTACT_US = constants.BASE_URL_TYPE + "contact_us";

  public static CARD_PURCHASE_ORDER = constants.BASE_URL_TYPE + "CardPurchaseOrder";

  public static ALL_BUSINESS_LIST = constants.BASE_URL_TYPE + "GetAllBusinessData";

  public static SUPPORT = constants.BASE_URL_TYPE + "Support";

  public static SEARCH = constants.BASE_URL_TYPE + "SearchBusinessWebsite";

  public static CountryList = constants.BASE_URL_TYPE + "CountryList";

  public static Signup = constants.BASE_URL_TYPE + "Signup";

  public static SCHOOL_SIGNUP = constants.BASE_URL_TYPE + "SchoolRegister";

  public static SEARCH_BUSINESS = constants.BASE_URL_TYPE + "searchBusinessWebsite/";

  public static ADS_BANNER = constants.BASE_URL_TYPE + "AdsBanner/";

  public static ABOUT_US = constants.BASE_URL_TYPE + "Pages";
}

export class storedConstants {
  public static USER_DETAILS = "MyRikshaew_user";
}
