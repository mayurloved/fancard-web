import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { HttpServiceService } from './services/http-service.service';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { ContactusComponent } from './components/contactus/contactus.component';

import { SignupSchoolComponent } from './signup-school/signup-school.component';
import { TimeFormatPipe } from './pipes/time-format.pipe';

@NgModule({
  imports: [CommonModule, RouterModule, ReactiveFormsModule],
  declarations: [

    HeaderComponent,
    FooterComponent,
    ContactusComponent,
    SignupSchoolComponent,
    TimeFormatPipe

  ],
  exports: [

    HeaderComponent,
    FooterComponent,
    ContactusComponent,
    SignupSchoolComponent,
    TimeFormatPipe

  ],
  providers: [HttpServiceService]
})
export class SharedModule { }
