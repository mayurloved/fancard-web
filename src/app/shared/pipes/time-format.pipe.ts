import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
@Pipe({
  name: 'timeFormat'
})
export class TimeFormatPipe implements PipeTransform {

  transform(time: any): any {
    var start, end;

    if (time.StartTime && time.EndTime) {
      if (time.StartTime != 'close' && time.EndTime != 'close') {
        start = moment(time.StartTime, "H:mm").format("h:mm a");
        end = moment(time.EndTime, "H:mm").format("h:mm a");
        return `${start} to ${end}`;
      }
      else if (time.StartTime == 'close' || time.EndTime == 'close') {
        return "Closed.";
      }
    }
    else {
      return "No time available.";
    }
  }

}
