import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpServiceService } from '../services/http-service.service';
import { constants } from '../constants';

@Component({
  selector: 'app-signup-school',
  templateUrl: './signup-school.component.html',
  styleUrls: ['./signup-school.component.scss']
})
export class SignupSchoolComponent implements OnInit {

  form;
  schoolSignupForm:FormGroup = null;
  schoolSignupFormSubmitted: any = false;
  responseText:any;
  constructor
  (
    public formBuilder: FormBuilder,
    public httpService : HttpServiceService
  ) 
  { }

  ngOnInit() {
    this.schoolSignupForm = this.formBuilder.group({
      schoolName: ["", Validators.required],
      firstName : ["", Validators.required],
      lastName : ["", Validators.required],
      emailAdd : ["",[Validators.required, Validators.email]],
      message : [""]
    });
  }

  get f() { return this.schoolSignupForm.controls; }


  submitData()
  {
    // console.log(this.schoolSignupForm);
    this.schoolSignupFormSubmitted = true;
    if(this.schoolSignupForm.valid){
      let formValues  = this.schoolSignupForm.value;
      //call api if valid
      var data = new FormData();
      data.append("SchoolName", formValues.schoolName);
      data.append("FirstName", formValues.firstName);
      data.append("LastName", formValues.lastName);
      data.append("Email", formValues.emailAdd);
      data.append("Message", formValues.message);
      this.httpService.postDataPromise(constants.SCHOOL_SIGNUP, data).then(detail => {
        // console.log(detail)
        let res:any = detail;
        if(res.status){
          // console.log(detail)
          this.responseText = res.message;
          setTimeout(() => {
            this.responseText = '';
          }, 5000);
          this.schoolSignupForm.reset();
          this.schoolSignupFormSubmitted = false;
        }else{
          // alert('cc')
        }
      })

    }else{
      return false;
    }
  }

}
