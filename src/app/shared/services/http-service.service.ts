import { Injectable } from '@angular/core';
import { constants } from '../constants';
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { SharedServiceService } from './shared-service.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpServiceService {
  BaseUrl = constants.BASE_URL;

  constructor(
    public http: HttpClient,
    private SharedService: SharedServiceService // private dataService: DataProvider
  ) {
    // console.log("Hello RestProvider Provider");
  }
  //Headers
  getHeader() {
    const httpOptions = {
      headers: new HttpHeaders({
        key: "fancard123*#*"
      })
    };

    return httpOptions;
    // }
  }
  ///observables
  getDataObservable(url, options = {}): Observable<any> {
    // this.SharedService.showLoading();
    options = this.getHeader();
    // console.log(this.getHeader());
    url = this.BaseUrl + url;
    return this.http.get(url, options);
  }
  postDataObservable(url, body, options?): Observable<any> {
    let httpOptions = this.getHeader();
    url = this.BaseUrl + url;
    return this.http.post(url, body, httpOptions);
  }
  putDataObservable(url, body = {}, options = {}): Observable<any> {
    url = this.BaseUrl + url;
    return this.http.put(url, body, options);
  }

  deleteDataObservable(url, options = {}): Observable<any> {
    url = this.BaseUrl + url;
    return this.http.delete(url, options);
  }
  ///Promises
  getDataPromise(url, options = {}): Promise<{}> {
    this.SharedService.showLoading();
    url = this.BaseUrl + url;
    return new Promise(resolve => {
      this.http.get(url, options).subscribe(
        data => {
          resolve(data);
          this.SharedService.hideLoading();
        },
        err => {
          // console.log(err);
          this.SharedService.hideLoading();
        }
      );
    });
  }

  postDataPromise(url, body, options = {}): Promise<{}> {
    let httpOptions = this.getHeader();
    // console.log(httpOptions);
    // console.log(body);
    url = this.BaseUrl + url;
    return new Promise(resolve => {
      this.http.post(url, body, httpOptions).subscribe(
        data => {
          resolve(data);
        },
        err => {
          // console.log(err);
        }
      );
    });
  }

  putDataPromise(url, body = {}, options = {}): Promise<{}> {
    url = this.BaseUrl + url;
    return new Promise(resolve => {
      this.http.put(url, body, options).subscribe(
        data => {
          resolve(data);
        },
        err => {
          // console.log(err);
        }
      );
    });
  }
}
