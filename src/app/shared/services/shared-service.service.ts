import { Injectable } from '@angular/core';
import { storedConstants } from '../constants';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ToastrService } from 'ngx-toastr';


@Injectable({
  providedIn: 'root'
})
export class SharedServiceService {


  isLoading: boolean = false;

  searchedData:any=[];
  search_title:any;
  categotyId:any;

  private user = new BehaviorSubject(null);
  public user$ = this.user.asObservable();
  constructor(public http: HttpClient, private toastr: ToastrService) { }

  showLoading() {
    // console.log("loder start");
    setTimeout(() => {
      this.isLoading = true;
    }, 1);
  }

  hideLoading() {
    // console.log("loder stop");

    this.isLoading = false;
  }

  //to store in local storage
  public localeStorage(key, data) {
    localStorage[key] = data;
  }
  public getFromLocalStorage(key) {
    let res = key ? JSON.parse(localStorage.getItem(key)) : null;
    return res;
  }
  public clearLocalStorage() {
    localStorage.clear();
  }
  //is logged in user

  checkForLoginStatus() {
    if (this.getFromLocalStorage(storedConstants.USER_DETAILS)) {
      this.user.next(this.getFromLocalStorage(storedConstants.USER_DETAILS));
      return this.getFromLocalStorage(storedConstants.USER_DETAILS);
    } else {
      this.user.next(null);
      return false;
    }
  }

  // toaster
  showToaster(msg, type?) {
    if (type) {
      if (type == "success") {
        this.toastr.success(msg, "My Rickshaw");
      }
      if (type == "error") {
        this.toastr.error(msg, "My Rickshaw");
      }
    } else {
      this.toastr.show(msg, "My Rickshaw");
    }
  }

  //Get current location 

  getPosition(): Promise<any> {
    
    var options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0
    };
    return new Promise((resolve, reject) => {

      navigator.geolocation.getCurrentPosition((resp) => {

        resolve({ lng: resp.coords.longitude, lat: resp.coords.latitude });
        // console.log(resp);
        // alert(resp)
      },(err:any) => {
        
          reject(err);
        },options);
    });

    

  }
}
