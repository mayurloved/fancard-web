import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages/pages.component';
import { HomeComponent } from './pages/home/home.component';
import { SchoolDetailComponent } from './pages/school-detail/school-detail.component';
import { ResturantDetailComponent } from './pages/resturant-detail/resturant-detail.component';
import { BusinessListComponent } from './pages/business-list/business-list.component';
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { SchoolComponent } from './pages/school/school.component';
import { BusinessesComponent } from './pages/businesses/businesses.component';
import { DealsListComponent } from './pages/deals-list/deals-list.component';
import { SupportComponent } from './pages/support/support.component';
import { SearchDataComponent } from './pages/search-data/search-data.component';
import { TAndcComponent } from './pages/t-andc/t-andc.component';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { PrivacyTermsComponent } from './pages/privacy-terms/privacy-terms.component';
import { SchoolSignupComponent } from './pages/school-signup/school-signup.component';
import { ActiveYourCardComponent } from './pages/active-your-card/active-your-card.component';
import { CreateAccountComponent } from './pages/create-account/create-account.component';
import { LoginComponent } from './pages/login/login.component';
import { EventAttendedComponent } from './pages/event-attended/event-attended.component';
import { TermsConditionComponent } from './pages/terms-condition/terms-condition.component';
import { PrivacyPolicyComponent } from './pages/privacy-policy/privacy-policy.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { SchoolEventsComponent } from './pages/school-events/school-events.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { FaqComponent } from './pages/faq/faq.component';
import { PurchaseOrderComponent } from './pages/purchase-order/purchase-order.component';



const routes: Routes = [
  {
    path: "",
    component: PagesComponent,
    children: [
      { path: "", component: HomeComponent },
      { path: "school/:id", component: SchoolDetailComponent },
      { path: "resturant/:id", component: ResturantDetailComponent },
      { path: "business-signup", component: BusinessesComponent },
      { path: "businesses/:id", component: BusinessListComponent },
      { path: "about-us", component: AboutUsComponent },
      { path: "schools", component: SchoolComponent },
      { path: 'deals', component: DealsListComponent },
      { path: 'support', component: SupportComponent },
      { path: 'search', component: SearchDataComponent },
      { path: 'terms-conditions', component: TAndcComponent },
      { path: 'contact-us', component: ContactUsComponent },
      { path: 'privacy-terms', component: PrivacyTermsComponent },
      { path: 'school-signup', component: SchoolSignupComponent },
      { path: 'active-your-card', component: ActiveYourCardComponent },
      { path: 'create-account', component: CreateAccountComponent },
      { path: 'login', component: LoginComponent },
      { path: 'event-attended', component: EventAttendedComponent},
      { path: 'terms-condition', component: TermsConditionComponent},
      { path: 'privacy-policy', component: PrivacyPolicyComponent},
      { path: 'profile', component: ProfileComponent},
      { path: 'school-events', component:SchoolEventsComponent},
      { path: 'change-password', component:ChangePasswordComponent},
      { path: 'faq', component:FaqComponent},
      { path: 'purchase-order', component: PurchaseOrderComponent }
    ]
  }

];


@NgModule({
  imports: [RouterModule.forRoot(routes, {
    enableTracing: false,
    useHash: false,
    initialNavigation: 'enabled',
    paramsInheritanceStrategy: 'always'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
