import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveYourCardComponent } from './active-your-card.component';

describe('ActiveYourCardComponent', () => {
  let component: ActiveYourCardComponent;
  let fixture: ComponentFixture<ActiveYourCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiveYourCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveYourCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
