import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpServiceService } from 'src/app/shared/services/http-service.service';
import { constants } from 'src/app/shared/constants';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {

  form;
  contactForm:FormGroup;
  contactFormSubmitted: any = false;
  responseText:any;
  
  constructor
  (
    public formBuilder: FormBuilder,
    public httpService : HttpServiceService
  ) { }

  ngOnInit() {
    this.contactForm = this.formBuilder.group({
      first_name:['',Validators.required],
      last_name:['',Validators.required],
      emailAddress: ['', [Validators.required, Validators.email]],
      message: ['', Validators.required]
    });
  }

  contactUsSubmit(){
    this.contactFormSubmitted = true;
    if(this.contactForm.valid){
      let formValues  = this.contactForm.value;
      //call api if valid
      var data = new FormData();
      data.append("first_name", formValues.first_name);
      data.append("last_name", formValues.last_name);
      data.append("email", formValues.emailAddress);
      data.append("message", formValues.message);
      this.httpService.postDataObservable(constants.CONTACT_US, data).subscribe(detail => {
        let res:any = detail
        if (detail.status) {
          // console.log(detail)
          this.responseText = detail.message;
          setTimeout(() => {
            this.responseText = '';
          }, 5000);
          this.contactForm.reset();
          this.contactFormSubmitted = false;
        }
      })

    }else{
      return false;
    }
  }
}
