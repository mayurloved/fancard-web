import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { HttpServiceService } from 'src/app/shared/services/http-service.service';
import { constants } from 'src/app/shared/constants';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { SharedServiceService } from 'src/app/shared/services/shared-service.service';

declare var $: any;
declare var jquery: any;
declare var google: any;

@Component({
  selector: 'app-resturant-detail',
  templateUrl: './resturant-detail.component.html',
  styleUrls: ['./resturant-detail.component.scss']
})
export class ResturantDetailComponent implements OnInit {

  map: any;
  detailList: any = [];
  CaresoulimgArray: any = []
  rating: any;
  businessId: any;
  lat: any;
  lng: any;

  searchData: any = [];
  currentUrl: any;


  @ViewChild('map') public mapElement: ElementRef;

  constructor
    (
      public route: Router,
      public httpService: HttpServiceService,
      public activeRoute: ActivatedRoute,
      public sharedService: SharedServiceService
    ) {

    this.businessId = this.activeRoute.snapshot.paramMap.get('id');
    this.currentUrl = this.route.url;
    // console.log(this.route.url)


  }

  ngOnInit() {
    this.LoadDetailView();
  }

  LoadDetailView() {

    this.httpService.getDataObservable(constants.BUSINESS_DETAIL + this.businessId)
      .subscribe(
        data => {


          if (data) {
            let res: any = [];
            res.push(data.mapData);
            this.detailList = res;
            this.CaresoulimgArray = this.detailList[0].BusinassImage;

            /*  Dynamic active class of indicators....*/
            for (var i = 0; i < this.CaresoulimgArray.length; i++) {
              $('<li data-target="#carousel-example-generic" data-slide-to="' + i + '"></li>').appendTo('.carousel-indicators')

            }
            $('.item').first().addClass('active');
            $('.carousel-indicators > li').first().addClass('active');
            $('#carousel-example-generic').carousel();

            this.rating = Math.round(this.detailList[0].Rating * 2) / 2;


            this.lat = this.detailList[0].BusinessLatitude;
            this.lng = this.detailList[0].BusinessLongitude;
            // console.log( this.lat,this.lng,this.detailList[0].MarkerImage)
            this.loadMap(this.lat, this.lng, this.detailList[0].MarkerImage, this.detailList[0].Website);

          } else {
            this.detailList = [];
            // this.loadMap(0,0,'');
          }


        }, err => {
          this.detailList = [];
          this.loadMap(0, 0, '', '');


        });
  }



  loadMap(lat, lng, MarkerImage, webLink) {
    let latLng = new google.maps.LatLng(lat, lng);

    let mapOptions = {
      center: latLng,
      zoom: 10,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      fullscreenControl: false
    }

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.addMarker(this.map, lat, lng, MarkerImage, webLink);
  }

  addMarker(map: any, lat, lng, MarkerImage, webLink) {
    var pinIcon = new google.maps.MarkerImage(
      MarkerImage,
      null, /* size is determined at runtime */
      null, /* origin is 0,0 */
      null, /* anchor is bottom center of the scaled image */
      new google.maps.Size(50, 55)
    );
    let marker = new google.maps.Marker({
      map: map,
      animation: google.maps.Animation.DROP,
      position: map.getCenter(lat, lng),
      clickable: true,
      icon: pinIcon
    });
    marker.addListener('click', (e) => {

      window.open(webLink, '_blank');

    });
  }

  openLink(webLink) {
    window.open(webLink, '_blank');
  }

}
