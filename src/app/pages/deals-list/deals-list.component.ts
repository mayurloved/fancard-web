import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { flatMap } from 'rxjs/operators';
import { HttpServiceService } from 'src/app/shared/services/http-service.service';
import { constants } from 'src/app/shared/constants';
import { Router } from '@angular/router';
import { SharedServiceService } from 'src/app/shared/services/shared-service.service';


declare var google: any;

@Component({
	selector: 'app-deals-list',
	templateUrl: './deals-list.component.html',
	styleUrls: ['./deals-list.component.scss']
})
export class DealsListComponent implements OnInit {


	processingForCategory:any = false;
	categoryArray: any = [];
	allCategories: any = [];
	showCategoryMore: boolean;
	
	// map: any;
	// foodCardArray: any = [];
	// showMoreData: boolean = true;
	// pageNo = 1;


	// schoolData: any;

	@ViewChild('map') public mapElement: ElementRef;

	constructor
		(
			private httpService: HttpServiceService,
			private sharedService: SharedServiceService,
			public router: Router
		) {


	}

	ngOnInit() {
		this.processingForCategory = true;
		this.getCategories();
	

		// this.sharedService.getPosition().then(pos => {
		// 	this.getSchoolDetail(this.pageNo, pos.lat, pos.lng);
		// }).catch(err => {
		// 	this.getSchoolDetail(this.pageNo, 0.000000, 0.000000);
		// });

	}

	// getSchoolDetail(pageNo: any, lat, lng) {
	// 	var data = new FormData();
	// 	data.append("Latitude", lat);
	// 	data.append("Longitude", lng);
	// 	data.append("Page", pageNo.toString());
	// 	this.httpService.postDataObservable(constants.ALL_BUSINESS_LIST, data).subscribe(detail => {

	// 		let array: any = []
	// 		array = detail.businessData;

	// 		if (array.length > 0) {

	// 			for (let i = 0; i < array.length; i++) {
	// 				this.foodCardArray.push(array[i]);
	// 			}

	// 			this.loadMap(this.foodCardArray[0].BusinessLatitude, this.foodCardArray[0].BusinessLongitude);
	// 			this.foodCardArray.forEach(element => {
	// 				element.Rating = Math.round(element.Rating * 2) / 2;
	// 				this.addMarker(this.map, element.BusinessLatitude, element.BusinessLongitude, element.MarkerImage);
	// 			});

	// 		} else {
	// 			this.loadMap(0, 0);
	// 			this.showMoreData = false;
	// 		}
	// 	})

	// }


	// showMoreDataList() {

	// 	this.pageNo = this.pageNo + 1;
	// 	this.sharedService.getPosition().then(pos => {
	// 		this.getSchoolDetail(this.pageNo, pos.lat, pos.lng);
	// 	}).catch(err => {
	// 		this.getSchoolDetail(this.pageNo, 0.000000, 0.000000);
	// 	});
	// }



	// loadMap(lat, long) {

	// 	let latLng = new google.maps.LatLng(lat, long);

	// 	let mapOptions = {
	// 		center: latLng,
	// 		zoom: 5,
	// 		mapTypeId: google.maps.MapTypeId.ROADMAP,
	// 		mapTypeControl: false,
	// 		fullscreenControl: false
	// 	}

	// 	this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
	// 	this.addMarker(this.map, lat, long, '');
	// }

	// addMarker(map: any, lat, lng, Marker_Image) {
	// 	let latLng = new google.maps.LatLng(lat, lng);


	// 	var pinIcon = new google.maps.MarkerImage(
	// 		Marker_Image,
	// 		null,
	// 		null,
	// 		null,
	// 		new google.maps.Size(50, 55)
	// 	);
	// 	let marker = new google.maps.Marker({
	// 		map: map,
	// 		animation: google.maps.Animation.DROP,
	// 		position: latLng,
	// 		clickable: true,
	// 		icon: pinIcon
	// 	});

	// }


	// gotoResturentDetailPage(business_id) {
	// 	this.router.navigateByUrl('/resturant/' + business_id);
	// }

	getCategories() {
		this.httpService.getDataObservable(constants.CATEGORIES).subscribe(categories => {

			if (categories && categories.categoryData) {
	  
			  this.allCategories = categories.categoryData;
			//   this.categoryArray = this.allCategories.length > 8 ? this.allCategories.slice(0, 8) : this.allCategories;
			//   this.showCategoryMore = this.allCategories.length > 8;
			 
			  this.processingForCategory = false;
			} else {
			  this.processingForCategory = false;
	  
			}
		  }, err => {
			
			this.processingForCategory = false;
	  
		  }
		  )
	}

	categoryViewMore() {
		this.showCategoryMore = false;
		this.categoryArray = this.allCategories;
	}

	gotoBusinessList(item) {
		this.router.navigateByUrl('/businesses/' + item.id);
  }
}

