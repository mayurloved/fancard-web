import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { PagesComponent } from './pages.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { SchoolDetailComponent } from './school-detail/school-detail.component';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { ResturantDetailComponent } from './resturant-detail/resturant-detail.component';
import { BusinessListComponent } from './business-list/business-list.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { SchoolComponent } from './school/school.component';
import { BusinessesComponent } from './businesses/businesses.component';
import { DealsListComponent } from './deals-list/deals-list.component';
import { SupportComponent } from './support/support.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { Ng2CompleterModule } from "ng2-completer";

import { ShareButtonsModule } from '@ngx-share/buttons';
import { SearchDataComponent } from './search-data/search-data.component';
import { TAndcComponent } from './t-andc/t-andc.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { PrivacyTermsComponent } from './privacy-terms/privacy-terms.component';
import { SchoolSignupComponent } from './school-signup/school-signup.component';
import { ActiveYourCardComponent } from './active-your-card/active-your-card.component';
import { CreateAccountComponent } from './create-account/create-account.component';
import { LoginComponent } from './login/login.component';
import { EventAttendedComponent } from './event-attended/event-attended.component';
import { TermsConditionComponent } from './terms-condition/terms-condition.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { ProfileComponent } from './profile/profile.component';
import { SchoolEventsComponent } from './school-events/school-events.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { FaqComponent } from './faq/faq.component';
import { PurchaseOrderComponent } from './purchase-order/purchase-order.component';
@NgModule({
  imports: [CommonModule, SharedModule, RouterModule, NgxSkeletonLoaderModule, ReactiveFormsModule, Ng2CompleterModule, FormsModule,
    ShareButtonsModule.withConfig({
      debug: true,
    })],
  declarations: [
    HomeComponent,
    PagesComponent,
    SchoolDetailComponent,
    ResturantDetailComponent,
    BusinessListComponent,
    AboutUsComponent,
    SchoolComponent,
    BusinessesComponent,
    DealsListComponent,
    SupportComponent,
    SearchDataComponent,
    TAndcComponent,
    ContactUsComponent,
    PrivacyTermsComponent,
    SchoolSignupComponent,
    ActiveYourCardComponent,
    CreateAccountComponent,
    LoginComponent,
    EventAttendedComponent,
    TermsConditionComponent,
    PrivacyPolicyComponent,
    ProfileComponent,
    SchoolEventsComponent,
    ChangePasswordComponent,
    FaqComponent,
    PurchaseOrderComponent
  ],
  exports: [
    HomeComponent,
    PagesComponent,
  ],
  providers: []
})
export class PagesModule { }
