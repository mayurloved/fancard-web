import { Component, OnInit } from '@angular/core';
import { SharedServiceService } from '../shared/services/shared-service.service';
import { Router, NavigationEnd } from '@angular/router';

declare var $: any;
declare var jquery: any;

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {

  constructor(public sharedServiceService: SharedServiceService,public router : Router) { }

  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }
      window.scrollTo(0, 0)
  });
  }

}
