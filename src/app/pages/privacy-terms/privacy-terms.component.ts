import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-privacy-terms',
  templateUrl: './privacy-terms.component.html',
  styleUrls: ['./privacy-terms.component.scss']
})
export class PrivacyTermsComponent implements OnInit {

  liList:any=[]
  OlList:any=[];
  liNextList:any=[];
  pList:any=[];
  pNextList:any=[];
  constructor() { }

  ngOnInit() {
    this.liList = [
      {
        text:'publishing any 1fancard.com material in any other media;'
      },
      {
        text:'selling, sublicensing and/or otherwise commercializing any 1fancard.com material;'
      },
      {
        text:'publicly performing and/or showing any 1fancard.com material;'
      },
      {
        text:'using 1fancard.com in any way that is or may be damaging to 1fancard.com;'
      },
      {
        text:'using 1fancard.com in any way that impacts user access to 1fancard.com;'
      },
      {
        text:'using 1fancard.com contrary to applicable laws and regulations, or in any way may cause harm to 1fancard.com, or to any person or business entity;'
      },
      {
        text:'engaging in any data mining, data harvesting, data extracting or any other similar activity in relation to 1fancard.com.'
      },
      {
        text:'All user ID and passwords you may have for 1fancard.com are confidential and you must maintain confidentiality as well.'
      }
    ]
    this.OlList = [
      {
        text:'1fancard.com is provided “as is,” with all faults, and One Team America, LLC express no representations or warranties, of any kind related to 1fancard.com or the materials contained on 1fancard.com. Also, nothing contained on this 1fancard.com shall be interpreted as advising you.'
      },
      {
        text:'In no event shall One Team America, LLC, nor any of its officers, directors and employees, shall be held liable for anything arising out of or in any way connected with your use of this Website whether such liability is under contract.  One Team America, LLC, including its officers, directors and employees shall not be held liable for any indirect, consequential or special liability arising out of or in any way related to your use of this Website.'
      },
      {
        text:'You hereby indemnify to the fullest extent One Team America, LLC from and against any and/or all liabilities, costs, demands, causes of action, damages and expenses arising in any way related to your breach of any of the provisions of these Terms.'
      },
      {
        text:'If any provision of these Terms is found to be invalid under any applicable law, such provisions shall be deleted without affecting the remaining provisions herein.'
      },
      {
        text:'One Team America, LLC is permitted to revise these Terms at any time as it sees fit, and by using 1fancard.com you are expected to review these Terms on a regular basis.'
      },
      {
        text:'The One Team America, LLC is allowed to assign, transfer, and subcontract its rights and/or obligations under these Terms without any notification. However, you are not allowed to assign, transfer, or subcontract any of your rights and/or obligations under these Terms.'
      },
      {
        text:'These Terms constitute the entire agreement between One Team America, LLC and you in relation to your use of 1fancard.com, and supersede all prior agreements and understandings.'
      },
      {
        text:'These Terms will be governed by and interpreted in accordance with the laws of the State of Minnesota, and you submit to the non-exclusive jurisdiction of the state and federal courts located in Minnesota for the resolution of any disputes.'
      }

    ]

    this.pList = [
      {
        text:'We implement a variety of security measures when a user enters, submits, or accesses their information to maintain the safety of your personal information.'
      },
      {
        text:'All transactions are processed through a gateway provider and are not stored or processed on our servers.'
      },
      {
        text:"Do we use 'cookies'?'"
      },
      {
        text:'We do not use cookies for tracking purposes'
      },
      {
        text:"You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser settings. Since browser is a little different, look at your browser's Help Menu to learn the correct way to modify your cookies."
      },
      {
        text:'If you turn cookies off, some features will be disabled. that make your site experience more efficient and may not function properly.'
      },
      {
        text:'Third-party disclosure'
      },
      {
        text:"We do not sell, trade, or otherwise transfer to outside parties your Personally Identifiable Information unless we provide users with advance notice. This does not include website hosting partners and other parties who assist us in operating our website, conducting our business, or serving our users, so long as those parties agree to keep this information confidential. We may also release information when it's release is appropriate to comply with the law, enforce our site policies, or protect ours or others' rights, property or safety."
      },
      {
        text:'However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.'
      },
      {
        text:'Third-party links.'
      },
      {
        text:'Occasionally, at our discretion, we may include or offer third-party products or services on our website. These third-party sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.'
      },
      {
        text:'California Online Privacy Protection Act'
      },
      {
        text:"CalOPPA is the first state law in the nation to require commercial websites and online services to post a privacy policy. The law's reach stretches well beyond California to require any person or company in the United States (and conceivably the world) that operates websites collecting Personally Identifiable Information from California consumers to post a conspicuous privacy policy on its website stating exactly the information being collected and those individuals or companies with whom it is being shared. - See more at: http://consumercal.org/california-online-privacy-protection-act-caloppa/#sthash.0FdRbT51.dpuf"
      },
      {
        text:"According to CalOPPA, we agree to the following:\n Users can visit our site anonymously.\n Once this privacy policy is created, we will add a link to it on our home page or as a minimum, on the first significant page after entering our website.\n Our Privacy Policy link includes the word 'Privacy' and can be easily be found on the page specified above."
      },

    ]
    this.pNextList = [
      {
        text:"How does our site handle Do Not Track signals?\nWe honor Do Not Track signals and Do Not Track, plant cookies, or use advertising when a Do Not Track (DNT) browser mechanism is in place.\n\n\n Does our site allow third-party behavioral tracking?\nIt's also important to note that we allow third-party behavioral tracking"
      },
      {
        text:"COPPA (Children Online Privacy Protection Act)"
      },
      {
        text:"When it comes to the collection of personal information from children under the age of 13 years old, the Children's Online Privacy Protection Act (COPPA) puts parents in control. The Federal Trade Commission, United States' consumer protection agency, enforces the COPPA Rule, which spells out what operators of websites and online services must do to protect children's privacy and safety online."
      },
      {
        text:"We do not specifically market to children under the age of 13 years old."
      },
      {
        text:"Fair Information Practices"
      },
      {
        text:"The Fair Information Practices Principles form the backbone of privacy law in the United States and the concepts they include have played a significant role in the development of data protection laws around the globe. Understanding the Fair Information Practice Principles and how they should be implemented is critical to comply with the various privacy laws that protect personal information."
      },
      {
        text:"In order to be in line with Fair Information Practices we will take the following responsive action, should a data breach occur:"
      },
      
    ]
    this.liNextList = [
      {
        text:'Not use false or misleading subjects or email addresses.'
      },
      {
        text:'Identify the message as an advertisement in some reasonable way.'
      },
      {
        text:'Include the physical address of our business or site headquarters.'
      },
      {
        text:'Monitor third-party email marketing services for compliance, if one is used.'
      },
      {
        text:'Honor opt-out/unsubscribe requests quickly.'
      },
      {
        text:'Allow users to unsubscribe by using the link at the bottom of each email.'
      },
      ]
  }

}
