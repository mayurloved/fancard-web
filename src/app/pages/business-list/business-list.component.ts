import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { HttpServiceService } from "src/app/shared/services/http-service.service";
import { constants } from "src/app/shared/constants";
import { SharedServiceService } from "src/app/shared/services/shared-service.service";

import { CompleterService, CompleterData } from "ng2-completer";

declare var $: any;
declare var jquery: any;
declare var google: any;

@Component({
  selector: "app-business-list",
  templateUrl: "./business-list.component.html",
  styleUrls: ["./business-list.component.scss"],
})
export class BusinessListComponent implements OnInit {
  map: any;
  foodCardArray: any = [];
  processingForSchool = false;
  schoolList: any = [];
  page: any = 1;
  catId: any;
  showCategoryMore: boolean;

  @ViewChild("map") public mapElement: ElementRef;
  schoolData: any = [];
  // searchData: any = [];
  longitude: any;
  latitude: any;

  bannerImage: any = [];



  public searchStr: string;
  public dataService: CompleterData;

  protected searchDataList = [];

  constructor(
    private route: ActivatedRoute,
    private httpService: HttpServiceService,
    private sharedService: SharedServiceService,
    public router: Router,
    private completerService: CompleterService
  ) {
    this.catId = this.route.snapshot.paramMap.get("id");
    if (this.catId) {
      this.sharedService
        .getPosition()
        .then((pos) => {
          this.longitude = pos.lng;
          this.latitude = pos.lat;
          this.getSchoolDetail(this.catId, pos.lat, pos.lng);
        })
        .catch((err) => {
          this.getSchoolDetail(this.catId, 0.0, 0.0);
        });
    }

    // this.searchData = this.sharedService.searchedData;
  }

  ngOnInit() {
    this.processingForSchool = true;
    this.showCategoryMore = true;

    this.searchResturent();

    this.getBannerimage();
  }

  getBannerimage() {
    this.httpService
      .getDataObservable(constants.ADS_BANNER + this.catId)
      .subscribe(
        (data) => {
          let res: any = data;
          if (res.status) {
            this.bannerImage = res.ads;
          }
          console.log(data);
        },
        (err) => {}
      );
  }

  searchResturent() {
    this.processingForSchool = false;
    this.httpService
      .getDataObservable(constants.SEARCH_BUSINESS + this.catId)
      .subscribe(
        (data) => {
          let res: any = data;
          this.searchDataList = res.searches;
          this.dataService = this.completerService.local(
            this.searchDataList,
            "Businassname",
            "Businassname"
          );
        },
        (err) => {}
      );
  }

  getSchoolDetail(id, lat, lng) {
    var data = new FormData();
    data.append("Latitude", lat);
    data.append("Longitude", lng);
    data.append("CategoryId", id);
    data.append("Page", this.page);
    this.httpService
      .postDataObservable(constants.BUSINESSLIST, data)
      .subscribe((detail) => {
        if (detail.mapData.length > 0) {
          this.schoolData = detail.mapData[0];
          detail.mapData.map((item) => {
            this.foodCardArray.push(item);
          });

          // console.log(this.foodCardArray);

          this.foodCardArray.filter((key) => {
            if (key.CategoryId == "4") {
              this.schoolList.push(key);
              // console.log('key',key)
            } else {
              this.schoolList = [];
            }
          });

          // this.processingForSchool = false;
          this.loadMap(
            this.foodCardArray[0].BusinessLatitude,
            this.foodCardArray[0].BusinessLongitude
          );
          this.foodCardArray.forEach((element) => {
            element.Rating = Math.round(element.Rating * 2) / 2;
            this.addMarker(
              this.map,
              element.BusinessLatitude,
              element.BusinessLongitude,
              this.foodCardArray[0].MarkerImage,
              element.Businessid,
              element.CommingSoon
            );
          });
        } else {
          // this.loadMap(0, 0);
          // this.foodCardArray = [];
          this.processingForSchool = false;
          this.showCategoryMore = false;
        }
      });
  }

  onSelect(e) {
    if (e) {
      this.showCategoryMore = false;
      var data = new FormData();
      data.append("Id", e.originalObject.BId);
      data.append("Type", "business");
      // data.append("Latitude", this.latitude ? this.latitude : 0.0);
      // data.append("Longitude", this.longitude ? this.longitude : 0.0);
      // data.append("zipcode", "");

      this.httpService
        .postDataPromise(constants.SEARCH, data)
        .then((detail) => {
          let res: any = detail;
          if (res) {
            this.foodCardArray = res.searchData;

            this.loadMap(
              this.foodCardArray[0].BusinessLatitude,
              this.foodCardArray[0].BusinessLongitude
            );
            this.foodCardArray.forEach((element) => {
              element.Rating = Math.round(element.Rating * 2) / 2;
              this.addMarker(
                this.map,
                element.BusinessLatitude,
                element.BusinessLongitude,
                this.foodCardArray[0].MarkerImage,
                element.Businessid,
                element.CommingSoon
              );
            });
          } else {
            this.foodCardArray = [];
          }
        });
    } else {
      this.foodCardArray = []
      this.showCategoryMore = true;
      this.sharedService
        .getPosition()
        .then((pos) => {
          this.longitude = pos.lng;
          this.latitude = pos.lat;
          this.getSchoolDetail(this.catId, pos.lat, pos.lng);
        })
        .catch((err) => {
          this.getSchoolDetail(this.catId, 0.0, 0.0);
        });
    }
  }

  loadMap(lat, long) {
    let latLng = new google.maps.LatLng(lat, long);

    let mapOptions = {
      center: latLng,
      zoom: 10,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      fullscreenControl: false,
    };

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.addMarker(this.map, lat, long, "", "", "");
  }

  addMarker(map: any, lat, lng, markerImage, businessId, CommingSoon) {
    // console.log(businessId,CommingSoon);
    let latLng = new google.maps.LatLng(lat, lng);

    var pinIcon = new google.maps.MarkerImage(
      markerImage,
      null,
      null,
      null,
      new google.maps.Size(50, 55)
    );

    let marker = new google.maps.Marker({
      map: map,
      animation: google.maps.Animation.DROP,
      position: latLng,
      clickable: true,
      icon: pinIcon,
    });

    marker.addListener("click", (e) => {
      if (this.catId == "4") {
        if (CommingSoon == "1") {
          return false;
        } else {
          this.router.navigateByUrl("/school/" + businessId);
        }
      } else {
        this.router.navigateByUrl("/resturant/" + businessId);
      }
    });
  }

  gotoResturentDetailPage(business_id) {
    this.router.navigateByUrl("/resturant/" + business_id);
  }

  gotoSchoolDetailView(item) {
    if (item.CommingSoon == "1") {
      return false;
    } else {
      // this.router.navigateByUrl('/school/' + item.Businessid);
      this.router.navigateByUrl("/school-events");
    }
  }

  categoryViewMore() {
    this.page = this.page + 1;
    if (this.catId) {
      this.sharedService
        .getPosition()
        .then((pos) => {
          this.getSchoolDetail(this.catId, pos.lat, pos.lng);
        })
        .catch((err) => {
          // alert(err)
          this.getSchoolDetail(this.catId, 0.0, 0.0);
        });
    }
  }

  openBannerUrl(url) {
    window.open(url, '_blank')
  }
}
