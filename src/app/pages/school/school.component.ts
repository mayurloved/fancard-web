import { Component, OnInit } from '@angular/core';
import { SharedServiceService } from 'src/app/shared/services/shared-service.service';
import { HttpServiceService } from 'src/app/shared/services/http-service.service';
import { constants } from 'src/app/shared/constants';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-school',
  templateUrl: './school.component.html',
  styleUrls: ['./school.component.scss']
})
export class SchoolComponent implements OnInit {

  schoolList: any = []
  longitude:any;
  latitude:any;
  processingForSchool = false;
  showSchoolMore: boolean;

  constructor
  (
    private sharedService: SharedServiceService,
    public http : HttpServiceService,
    public router : Router
  ) 
  { }

  ngOnInit() {
    this.processingForSchool = true;
    this.sharedService.getPosition().then(pos => {
      this.longitude = pos.lng;
      this.latitude = pos.lat;
      this.getSchoolList();
    }).catch(err=>{
      this.getSchoolList();
    });

    

  }

  getSchoolList(){
    var data = new FormData();
    // console.log('lat long',this.latitude,this.longitude);

    data.append("Latitude", this.latitude ? this.latitude : 0.000000);
    data.append("Longitude", this.longitude ? this.longitude : 0.000000);
    data.append("Page", '0');

    this.http.postDataObservable(constants.SCHOOLLISTFROMLOCATION, data).subscribe(schools => {

      if (schools.status && schools.SchoolData && schools.SchoolData.length > 0) {
        let school = schools.SchoolData;
        if (school.length > 0) {
          this.showSchoolMore = school.length == 12;

          for (let i = 0; i < school.length; i++) {
            this.schoolList.push(school[i]);
          }
        }

       
        this.processingForSchool = false;
      } else {
        this.showSchoolMore = false;
        this.processingForSchool = false;

      }

    }, err => {
      // console.log(err);
      this.processingForSchool = false;

    });
  }

  gotoSchoolDetailView(item) {
    if(item.ComingSoon == "1"){
      return false;
    }else{
      this.router.navigateByUrl('/school/' + item.Businessid);
    }
    
  }

}
