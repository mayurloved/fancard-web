import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { SharedServiceService } from 'src/app/shared/services/shared-service.service';
import { Router } from '@angular/router';


declare var google: any;


@Component({
	selector: 'app-search-data',
	templateUrl: './search-data.component.html',
	styleUrls: ['./search-data.component.scss']
})
export class SearchDataComponent implements OnInit {

	searchTitle:any;
	map: any;
	searchData: any = [];
	businessdata: any = [];
	showMoreData: any;
	schoolData:any=[];

	
	processingForSchool = false;

	@ViewChild('map') public mapElement: ElementRef;

	constructor
		(
			public sharedService: SharedServiceService,
			public router: Router
		) {
		this.searchData = this.sharedService.searchedData[0].searchData;
		this.searchTitle = this.sharedService.search_title;
		
		

	}

	ngOnInit() {

		if (this.searchData.length > 0) {
		
			// console.log(this.businessdata);
			
			this.loadMap(this.searchData[0].BusinessLatitude, this.searchData[0].BusinessLongitude);

			this.searchData.forEach(element => {
				if(this.sharedService.categotyId == '1'){
					this.businessdata.push(element);
				}else if(this.sharedService.categotyId == '4') { 
					this.schoolData.push(element);
					this.processingForSchool = false;
				}
				// console.log('data',this.schoolData,this.businessdata);
				element.Rating = Math.round(element.Rating * 2) / 2;
				this.addMarker(this.map, element.BusinessLatitude, element.BusinessLongitude, element.MarkerImage,element.Businessid,element.CommingSoon);
			});

			this.showMoreData = false;

		}
		else{
			this.loadMap(0 ,0);
		}
	}

	loadMap(lat, long) {

		let latLng = new google.maps.LatLng(lat, long);

		let mapOptions = {
			center: latLng,
			zoom: 5,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			mapTypeControl: false,
			fullscreenControl: false
		}

		this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
		this.addMarker(this.map, lat, long, '','','');
	}

	addMarker(map: any, lat, lng, Marker_Image,Businessid,CommingSoon) {
		let latLng = new google.maps.LatLng(lat, lng);


		var pinIcon = new google.maps.MarkerImage(
			Marker_Image,
			null,
			null,
			null,
			new google.maps.Size(50, 55)
		);
		let marker = new google.maps.Marker({
			map: map,
			animation: google.maps.Animation.DROP,
			position: latLng,
			clickable: true,
			icon: pinIcon
		});

		marker.addListener('click', (e)=> {
			
			if(this.sharedService.categotyId  == "4"){
				if (CommingSoon == "1") {
					return false;
				} else {
					this.router.navigateByUrl('/school/' + Businessid);
				}
			}else{
				this.router.navigateByUrl('/resturant/' + Businessid);
			}
			
		  });


	}

	gotoResturentDetailPage(business_id) {
	
		this.router.navigateByUrl('/resturant/' + business_id);
	}

	gotoSchoolDetailView(item) {
		
		if(item.CommingSoon == "1"){
			return false;
		}else{
			this.router.navigateByUrl('/school/' + item.Businessid);
		}
		
	  }

}
