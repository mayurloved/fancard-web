import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { HttpServiceService } from 'src/app/shared/services/http-service.service';
import { constants } from 'src/app/shared/constants';

declare var $: any;
declare var jquery: any;


@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {

  aboutus_desc: any = [];
  quot_text: any;
  ceo_name: any;

  constructor
    (
      public httpService: HttpServiceService
    ) { }

  ngOnInit() {
    this.getDescription();
  }

  getDescription() {
    this.httpService.getDataObservable(constants.ABOUT_US)
      .subscribe(desc => {
        let res: any = desc;
        this.aboutus_desc = res.data;
        let text: String = this.aboutus_desc[0].sectioncontent;
        let result = text.replace('"', '\"');
        let quotes = result.split('---');
        this.quot_text = quotes[0];

        let ceoName = quotes[1].replace('"', '').replace("---", '');
        this.ceo_name = ceoName;
        // console.log('', result);
      }, err => {
        // console.log('err', err);
      })
  }





}
