import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { HttpServiceService } from 'src/app/shared/services/http-service.service';
import { constants } from 'src/app/shared/constants';
import { SharedServiceService } from 'src/app/shared/services/shared-service.service';
import { CompleterService, CompleterData } from 'ng2-completer';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {


  schoolList: any = []
  categoryArray: any = [];
  allCategories: any = [];

  showCategoryMore: boolean;
  processingForSchool = false;
  processingForCategory = false;
  pageNo = 1;
  longitude: any;
  latitude: any;
  showSchoolMore: boolean;
  errorText: any;
  sponsoredBusiness: any = [];
  sponsorPage = 1
  showMoreSponsor = false;
  allSponsored: any = []
  public searchStr: string;
  public dataService: CompleterData;


  protected searchData = [];


  constructor
    (
      public router: Router,
      private http: HttpServiceService,
      private sharedService: SharedServiceService,
      private completerService: CompleterService,

  ) {

  }

  ngOnInit() {

    this.processingForSchool = true;
    this.processingForCategory = true;
    this.getCategories();
    this.sharedService.getPosition().then(pos => {
      this.longitude = pos.lng;
      this.latitude = pos.lat;
      this.getSponsorBusiness(this.sponsorPage)
      this.getSchoolList();
    }).catch(err => {
      this.getSponsorBusiness(this.sponsorPage)
      this.getSchoolList();
    });
  }

  getCategories() {
    this.http.getDataObservable(constants.CATEGORIES).subscribe(categories => {

      if (categories && categories.categoryData) {

        this.allCategories = categories.categoryData;
        this.categoryArray = this.allCategories.length > 8 ? this.allCategories.slice(0, 8) : this.allCategories;
        this.showCategoryMore = this.allCategories.length > 8;

        this.searchData = categories.searches;

        // console.log(this.searchData)
        this.dataService = this.completerService.local(this.searchData, 'name', 'name');

        this.processingForCategory = false;
      } else {
        this.processingForCategory = false;

      }
    }, err => {

      this.processingForCategory = false;

    }
    )
  }

  categoryViewMore() {
    this.showCategoryMore = false;
    this.categoryArray = this.allCategories;
  }

  getSchoolList() {
    var data = new FormData();


    data.append("Latitude", this.latitude ? this.latitude : 0.000000);
    data.append("Longitude", this.longitude ? this.longitude : 0.000000);
    data.append("Page", this.pageNo.toString());

    this.http.postDataObservable(constants.SCHOOLLISTFROMLOCATION, data).subscribe(schools => {

      if (schools.status && schools.SchoolData && schools.SchoolData.length > 0) {
        let school = schools.SchoolData;
        if (school.length > 0) {
          this.showSchoolMore = school.length == 12;

          for (let i = 0; i < school.length; i++) {
            this.schoolList.push(school[i]);
          }
        }

        this.pageNo++;
        this.processingForSchool = false;
      } else {
        this.showSchoolMore = false;
        this.processingForSchool = false;

      }

    }, err => {

      this.processingForSchool = false;

    }
    )
  }

  getSponsorBusiness(page) {
    var data = new FormData();


    data.append("Latitude", this.latitude ? this.latitude : 0.000000);
    data.append("Longitude", this.longitude ? this.longitude : 0.000000);
    data.append("Page", page.toString());

    this.http.postDataObservable(constants.SPONSORED_BUSINESS, data).subscribe(business => {

      if (business.mapData.length > 0) {

        this.allSponsored = business.mapData;
        this.sponsoredBusiness = this.allSponsored.length > 6 ? this.allSponsored.slice(0, 6) : this.allSponsored;
        this.showMoreSponsor= this.allSponsored.length > 6;

        this.allSponsored.forEach((element) => {
          element.Rating = Math.round(element.Rating * 2) / 2;
        });
      } else {
        this.showMoreSponsor = false;
      }
    }, err => {
      console.log(err);

    }
    )
  }


  gotoSchoolDetailView(item) {
    if (item.ComingSoon == "1") {
      return false;
    } else {
      this.router.navigateByUrl('/school/' + item.Businessid);
    }


  }

  gotoBusinessList(item) {
    this.router.navigateByUrl('/businesses/' + item.id);
  }


  onSelect(e) {


    this.sharedService.searchedData = [];
    this.sharedService.search_title = '';
    this.sharedService.categotyId = '';

    // console.log(e);

    if (e.title && e.originalObject.categoryId) {
      this.sharedService.search_title = e.title;
      this.sharedService.categotyId = e.originalObject.categoryId;
      var data = new FormData();

      data.append("Id", e.originalObject.id);
      data.append("Type", e.originalObject.type);
      data.append("Latitude", this.latitude ? this.latitude : 0.000000);
      data.append("Longitude", this.longitude ? this.longitude : 0.000000);
      data.append("zipcode", '');
      this.http.postDataPromise(constants.SEARCH, data).then(detail => {
        let res: any = detail;
        if (res) {
          this.sharedService.searchedData.push(res);

          // console.log(this.sharedService.searchedData);

          this.router.navigateByUrl('/search');

        } else {
          this.errorText = 'error';
          setTimeout(() => {
            this.errorText = '';
          }, 1000);
          this.sharedService.searchedData = [];
        }
      })
    } else {
      this.sharedService.hideLoading();
      this.errorText = 'error';
      setTimeout(() => {
        this.errorText = '';
      }, 3000);
      this.sharedService.searchedData = [];
    }



  }

  gotoResturentDetailPage(business_id) {
    this.router.navigateByUrl("/resturant/" + business_id);
  }

  sponsoredViewMore() {
    this.sponsoredBusiness = this.allSponsored;
    this.showMoreSponsor = false
  }

}
