import { Component, OnInit } from '@angular/core';
import { HttpServiceService } from 'src/app/shared/services/http-service.service';
import { constants } from 'src/app/shared/constants';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


export class Country
{
    name:string;
    id:any;
}

@Component({
  selector: 'app-businesses',
  templateUrl: './businesses.component.html',
  styleUrls: ['./businesses.component.scss']
})
export class BusinessesComponent implements OnInit {

  form;
  BusinessForm: FormGroup;
  BusinessFormSubmitted: any = false
  responseText: any;
  agreee:any;
  // processingForCategory:any = false;
  // categoryArray: any = [];
  // allCategories: any = [];
  // showCategoryMore: boolean;
  countryname: Country = new Country();


  countryList: any = [];
  constructor
    (
      public http: HttpServiceService,
      public router: Router,
      public formBuilder: FormBuilder
    ) { }

  ngOnInit() {
    
    // this.processingForCategory = true;
    // this.getCategories();

    this.BusinessForm = this.formBuilder.group({
      business_name: ["", Validators.required],
      phno: ['', [Validators.required, Validators.pattern('^([0-9]{7})$|(^[0-9]{10})$')]],
      street_add: ["", Validators.required],
      street_add2: [""],
      city: ["", Validators.required],
      state_region: ["", Validators.required],
      Postal_zip: ["", Validators.required],
      country: ["", Validators.required],
      deal_desc: ["", Validators.required],
      agree: ["", Validators.required],
      first_name: ["", Validators.required],
      last_name: ["", Validators.required],
      emailAdd: ['', [Validators.required, Validators.email]],
      contactPhno: ['', [Validators.required, Validators.pattern('^([0-9]{7})$|(^[0-9]{10})$')]]
    });

    this.selectData();
  }


  get f() { return this.BusinessForm.controls; }


  selectData() {
    this.http.getDataObservable(constants.CountryList).subscribe(country => {

      // console.log(country);
      this.countryname.name = country.CountryData[230].country_name;
      this.countryname.id = country.CountryData[230].id;
      this.countryList = country.CountryData;

    });
  }
  // getCategories() {
  // this.http.getDataObservable(constants.CATEGORIES).subscribe(categories => {

  // if (categories && categories.categoryData) {

  // this.allCategories = categories.categoryData;
  // this.categoryArray = this.allCategories.length > 8 ? this.allCategories.slice(0, 8) : this.allCategories;
  // this.showCategoryMore = this.allCategories.length > 8;
  //     this.categoryArray = categories.categoryData;
  //     this.processingForCategory = false;
  //   } else {
  //     this.processingForCategory = false;

  //   }
  // }, err => {
  // console.log(err);
  //     this.processingForCategory = false;

  //   }
  //   )
  // }

  // categoryViewMore() {
  //   this.showCategoryMore = false;
  //   this.categoryArray = this.allCategories;
  // }

  // gotoBusinessList(item) {
  //   this.router.navigateByUrl('/businesses/' + item.id);
  // }


  submitData() {
    // console.log(this.BusinessForm);
    this.BusinessFormSubmitted = true;
    if (this.BusinessForm.valid) {
      let formValues = this.BusinessForm.value;


      //call api if valid
      var data = new FormData();
      data.append("BusinessName", formValues.business_name);
      data.append("Phone", formValues.phno);
      data.append("Address", formValues.street_add);

      data.append("Address2", formValues.street_add2);
      data.append("City", formValues.city);
      data.append("State", formValues.state_region);

      data.append("Zipcode", formValues.Postal_zip);
      data.append("Country", formValues.country);
      data.append("Description", formValues.deal_desc);

      data.append("CheckBox", formValues.agree);
      data.append("FirstName", formValues.first_name);
      data.append("LastName", formValues.last_name);


      data.append("Email", formValues.emailAdd);
      data.append("Number", formValues.contactPhno);
      
      this.http.postDataPromise(constants.Signup, data).then(detail => {
        // console.log(detail)
        let res: any = detail;
        if (res.status) {
          // console.log(detail)
          this.responseText = res.message;
          setTimeout(() => {
            this.responseText = '';
          }, 5000);
          // this.BusinessForm.reset();
          this.BusinessForm.reset();
          this.BusinessFormSubmitted = false;
        } else {
          // alert('cc')
        }
      })

    } else {
      return false;
    }
  }

}
