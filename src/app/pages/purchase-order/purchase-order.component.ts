import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { constants } from '../../shared/constants';
import { HttpServiceService } from 'src/app/shared/services/http-service.service';

@Component({
  selector: 'app-purchase-order',
  templateUrl: './purchase-order.component.html',
  styleUrls: ['./purchase-order.component.scss']
})
export class PurchaseOrderComponent implements OnInit {

  form;
  purchaseOrderForm: FormGroup;
  purchaseOrderFormSubmitted: any = false;
  responseText: any;

  constructor(
    public formBuilder: FormBuilder,
    public httpService: HttpServiceService

  ) { }

  ngOnInit() {
    this.purchaseOrderForm = this.formBuilder.group({
      high_school_name: ['', Validators.required],
      school_mobile_number: ['', [Validators.required]],
      high_school_email: ['', [Validators.required, Validators.email]],
      director_name: ['', Validators.required],
      shipping_details: ['', Validators.required],
      order_qty: ['', Validators.required],
      special_instruction: ['', Validators.required]
    });

  }

  orderSubmit() {
    this.purchaseOrderFormSubmitted = true;
    if (this.purchaseOrderForm.valid) {
      let formValues = this.purchaseOrderForm.value;
      //call api if valid
      var data = new FormData();
      data.append("SchoolName", formValues.high_school_name);
      data.append("MobileNumber", formValues.school_mobile_number);
      data.append("Email", formValues.high_school_email);
      data.append("DirectorName", formValues.director_name);
      data.append("ShippingAddress", formValues.shipping_details);
      data.append("Qty", formValues.order_qty);
      data.append("SpecialInstruction", formValues.special_instruction);
      this.httpService.postDataObservable(constants.CARD_PURCHASE_ORDER, data).subscribe(detail => {
        if (detail.status) {
          console.log(detail)
          this.responseText = detail.message;
          setTimeout(() => {
            this.responseText = '';
          }, 5000);
          this.purchaseOrderForm.reset();
          this.purchaseOrderFormSubmitted = false;
        }
      })

    } else {
      return false;
    }
  }

}
