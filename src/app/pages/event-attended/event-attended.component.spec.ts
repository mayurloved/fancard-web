import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventAttendedComponent } from './event-attended.component';

describe('EventAttendedComponent', () => {
  let component: EventAttendedComponent;
  let fixture: ComponentFixture<EventAttendedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventAttendedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventAttendedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
