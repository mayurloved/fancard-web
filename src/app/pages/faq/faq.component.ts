import { Component, OnInit } from '@angular/core';


declare var $: any;
declare var jquery: any;
@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {

  isshow: any = false;
  constructor() { }

  ngOnInit() {
    this.togglebtn();
  }

  togglebtn() {
    $(".collapse.show").each(function () {
      console.log('call')
      $(this).siblings(".hrBorder-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
    });
    $(".collapse").on('show.bs.collapse', function () {
      $(this).parent().find(".fa").removeClass("fa-plus").addClass("fa-minus");
    }).on('hide.bs.collapse', function () {
      $(this).parent().find(".fa").removeClass("fa-minus").addClass("fa-plus");
    });
  }

}
