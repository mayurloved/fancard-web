import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpServiceService } from 'src/app/shared/services/http-service.service';
import { constants } from 'src/app/shared/constants';
import { SharedServiceService } from 'src/app/shared/services/shared-service.service';



declare var google: any;


@Component({
  selector: 'app-school-detail',
  templateUrl: './school-detail.component.html',
  styleUrls: ['./school-detail.component.scss']
})
export class SchoolDetailComponent implements OnInit {


  map: any;
  foodCardArray: any = [];
  rating: any;
  @ViewChild('map') public mapElement: ElementRef;
  school: any = [];
  page = 1 ;
  eventData: any = [];
schoolId: any;
  constructor
    (
      private route: ActivatedRoute,
      private httpService: HttpServiceService,
      private sharedService: SharedServiceService,
      public router: Router
    ) {
    this.schoolId = this.route.snapshot.paramMap.get('id');
    if (this.schoolId) { this.getSchoolDetail() }
  }
  ngOnInit() {
    this.sharedService.getPosition().then(pos => {

      this.loadMap(pos.lat, pos.lng);
    }).catch(err => {
      this.loadMap(0.000000, 0.000000);
    })
  }

  onScroll(event: any) {
    if (
      event.target.offsetHeight + event.target.scrollTop >=
      event.target.scrollHeight
    ) {
      this.getSchoolDetail();
      console.log("End");
    }
  }

  getSchoolDetail() {
    this.httpService.getDataObservable(constants.SCHOOLDETAIL + this.schoolId + '/' + this.page).subscribe(detail => {
      let res: any = detail;
      this.page = this.page + 1;
      if (res.SchoolData) {
        this.school = detail.SchoolData;
        // this.school.push.apply(this.school, detail.SchoolData);
        // console.log(this.school,this.school.CardImage);
      } else {
        this.school = [];
      }
      if (res.OfferData.length > 0) {
        // this.foodCardArray = (detail.OfferData && detail.OfferData.length > 0) ? detail.OfferData : [];
        this.foodCardArray.push.apply(this.foodCardArray, detail.OfferData);

        this.loadMap(this.foodCardArray[0].Offerlatitude, this.foodCardArray[0].Offerlongitude)

        this.foodCardArray.forEach(element => {
          element.Rating = Math.round(element.Rating * 2) / 2;
          this.addMarker(this.map, element.Offerlatitude, element.Offerlongitude, element.MarkerImage, element.Businessid);

        });
      } else {
        this.loadMap(0.000000, 0.000000);
        this.foodCardArray = [];
      }
      if (res.EventData) {
        // this.eventData = detail.EventData;
        this.eventData.push.apply(this.eventData, detail.EventData);
        // console.log(this.school,this.school.CardImage);
      } else {
        this.eventData = [];
      }
    })

  }

  loadMap(lat, long) {

    let latLng = new google.maps.LatLng(lat, long);

    let mapOptions = {
      center: latLng,
      zoom: 10,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      mapTypeControl: false,
      fullscreenControl: false
    }

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.addMarker(this.map, lat, long, '', '');
  }

  addMarker(map: any, lat, lng, markerimage, Businessid) {
    let latLng = new google.maps.LatLng(lat, lng);


    var pinIcon = new google.maps.MarkerImage(
      markerimage,
      null,
      null,
      null,
      new google.maps.Size(50, 55)
    );
    let marker = new google.maps.Marker({
      map: map,
      animation: google.maps.Animation.DROP,
      position: latLng,
      clickable: true,
      icon: pinIcon
    });

    marker.addListener('click', (e) => {

      this.router.navigateByUrl('/resturant/' + Businessid);


    });

  }

  gotoDetailPage(Businessid) {
    this.router.navigateByUrl('/resturant/' + Businessid);
  }
}
